package org.example.archive;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.example.shop.Item;
import org.example.shop.Order;

class PurchasesArchiveTest {
    private PurchasesArchive archive;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @BeforeEach
    void setUp() {
        archive = new PurchasesArchive();
        // Přesměrování výstupu na standardní výstup do proměnné pro testování
        System.setOut(new PrintStream(outContent));
    }

    @Test
    void testPrintItemPurchaseStatistics() {
        // Inicializace archivu pro testování
        PurchasesArchive archive = new PurchasesArchive();

        // Nastavení výstupu do ByteArrayOutputStream pro zachycení výstupů systému
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Mockování objektu Item
        Item item = mock(Item.class);
        when(item.toString()).thenReturn("Item1");

        // Mockování objektu ItemPurchaseArchiveEntry
        ItemPurchaseArchiveEntry entry = mock(ItemPurchaseArchiveEntry.class);
        when(entry.toString()).thenReturn("Item1 statistics");

        // Přidání mockovaného záznamu do mapy v archive
        archive.itemPurchaseArchive.put(1, entry);

        // Volání metody, která má být testována
        archive.printItemPurchaseStatistics();

        // Převedení výstupu do stringu a normalizace konců řádků
        String actualOutput = outContent.toString().replace(System.lineSeparator(), "\n");

        // Definování očekávaného výstupu s normalizovanými konci řádků
        String expectedOutput = "ITEM PURCHASE STATISTICS:\nItem1 statistics\n";

        // Porovnání očekávaného výstupu se skutečným
        assertEquals(expectedOutput, actualOutput);

        // Resetování standardního výstupu
        System.setOut(System.out);
    }


    @Test
    void testGetHowManyTimesHasBeenItemSold_ExistingItem() {
        // Příprava objektu Item a nastavení ID
        Item item = mock(Item.class);
        when(item.getID()).thenReturn(1);
        ItemPurchaseArchiveEntry entry = mock(ItemPurchaseArchiveEntry.class);
        when(entry.getCountHowManyTimesHasBeenSold()).thenReturn(5);
        archive.itemPurchaseArchive.put(1, entry);

        // Získání počtu prodaných kusů
        int soldCount = archive.getHowManyTimesHasBeenItemSold(item);

        // Kontrola, zda počet odpovídá očekávání
        assertEquals(5, soldCount);
    }

    @Test
    void testGetHowManyTimesHasBeenItemSold_NonExistingItem() {
        // Příprava neexistujícího itemu
        Item item = mock(Item.class);
        when(item.getID()).thenReturn(999);

        // Získání počtu prodaných kusů pro neexistující item
        int soldCount = archive.getHowManyTimesHasBeenItemSold(item);

        // Kontrola, zda výsledek je nula
        assertEquals(0, soldCount);
    }

    @Test
    void testPutOrderToPurchasesArchive_NewAndExistingItems() {
        // Příprava itemů a nastavení ID
        Item item1 = mock(Item.class);
        when(item1.getID()).thenReturn(1);
        Item item2 = mock(Item.class);
        when(item2.getID()).thenReturn(2);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item1);
        items.add(item2);
        Order order = mock(Order.class);
        when(order.getItems()).thenReturn(items);

        // Vložení objednávky do archivu
        archive.putOrderToPurchasesArchive(order);

        // Kontrola, zda je objednávka v archivu a zda byly zaznamenány prodeje
        assertTrue(archive.orderArchive.contains(order));
        assertEquals(1, archive.itemPurchaseArchive.get(1).getCountHowManyTimesHasBeenSold());
        assertEquals(1, archive.itemPurchaseArchive.get(2).getCountHowManyTimesHasBeenSold());
    }
}
