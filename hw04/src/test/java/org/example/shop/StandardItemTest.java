package org.example.shop;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Enclosed.class)
public class StandardItemTest {

    // Test konstruktoru
    @Test
    public void testConstructor() {
        StandardItem item = new StandardItem(1, "Test Item", 100.0f, "Electronics", 10);
        assertEquals(1, item.getID());
        assertEquals("Test Item", item.getName());
        assertEquals(100.0f, item.getPrice(), 0.01);
        assertEquals("Electronics", item.getCategory());
        assertEquals(10, item.getLoyaltyPoints());
    }

    // Test metody copy
    @Test
    public void testCopy() {
        StandardItem original = new StandardItem(1, "Test Item", 100.0f, "Electronics", 10);
        StandardItem copy = original.copy();
        assertEquals(original.getID(), copy.getID());
        assertEquals(original.getName(), copy.getName());
        assertEquals(original.getPrice(), copy.getPrice(), 0.01);
        assertEquals(original.getCategory(), copy.getCategory());
        assertEquals(original.getLoyaltyPoints(), copy.getLoyaltyPoints());
    }

    // Parametrizovaný test pro metodu equals
    @RunWith(Parameterized.class)
    public static class StandardItemEqualsTest {
        private StandardItem item1;
        private StandardItem item2;
        private boolean expected;

        public StandardItemEqualsTest(StandardItem item1, StandardItem item2, boolean expected) {
            this.item1 = item1;
            this.item2 = item2;
            this.expected = expected;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][] {
                    { new StandardItem(1, "Test Item", 100.0f, "Electronics", 10), new StandardItem(1, "Test Item", 100.0f, "Electronics", 10), true },
                    { new StandardItem(1, "Test Item", 100.0f, "Electronics", 10), new StandardItem(2, "Test Item", 100.0f, "Electronics", 10), false },
                    { new StandardItem(1, "Test Item", 100.0f, "Electronics", 10), new StandardItem(1, "Test Item", 100.0f, "Electronics", 20), false },
                    { new StandardItem(1, "Test Item", 100.0f, "Electronics", 10), null, false }
            });
        }

        @Test
        public void testEquals() {
            assertEquals(expected, item1.equals(item2));
        }
    }
}
