package org.example.shop;

import org.example.archive.PurchasesArchive;
import org.example.storage.NoItemInStorage;
import org.example.storage.Storage;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;



public class EShopControllerTest {

    private Storage storage;
    private PurchasesArchive archive;
    private EShopController controller;

    @Before
    public void setUp() {
        controller = new EShopController();
        EShopController.startEShop();
        storage = EShopController.getStorage(); // Předpokládáme, že je k dispozici getter pro sklad v EShopController
        archive = new PurchasesArchive();
    }

    @Test(expected = NoItemInStorage.class)
    public void testPurchaseWithInsufficientStock() throws NoItemInStorage {
        ShoppingCart cart = EShopController.newCart();
        StandardItem tool = new StandardItem(3, "Screwdriver", 200, "TOOLS", 5);
        storage.insertItems(tool, 1); // Přidáme 5 šroubováků do skladu.
        cart.addItem(tool);
        cart.addItem(tool); // Přidáme šroubovák dvakrát do košíku.
        // Očekáváme vyvolání výjimky, protože v košíku je 2x šroubovák, ale skladová zásoba je pouze 1.
        EShopController.purchaseShoppingCart(cart, "Libuse Novakova", "Kosmonautu 25, Praha 8");
    }

    @Test
    public void testTotalPriceCalculation() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5));
        cart.addItem(new StandardItem(2, "Screwdriver", 200, "TOOLS", 5));
        // Ověříme, že celková cena položek v košíku odpovídá očekávané hodnotě.
        assertEquals("Celková cena by měla být 5200.", 5200, cart.getTotalPrice());
    }

    @Test
    public void testOrderProcessing() throws NoItemInStorage {
        ShoppingCart cart = new ShoppingCart();
        StandardItem screwdriver = new StandardItem(3, "Screwdriver", 200, "TOOLS", 5);
        storage.insertItems(screwdriver, 10); // Přidáme 10 šroubováků do skladu.
        cart.addItem(screwdriver); // Přidáme jeden šroubovák do košíku.

        // Provedeme nákup, který by měl snížit počet šroubováků na skladě o jeden.
        EShopController.purchaseShoppingCart(cart, "Karel Nový", "Dlouhá 100, Brno");
        assertEquals("Počet šroubováků na skladě by měl být 9.", 9, storage.getItemCount(screwdriver.getID()));
    }

    @Test
    public void testDynamicChangesInCartBeforePurchase() throws NoItemInStorage {
        ShoppingCart cart = EShopController.newCart();
        StandardItem panda = new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5);
        StandardItem screwdriver = new StandardItem(3, "Screwdriver", 200, "TOOLS", 5);
        storage.insertItems(panda, 5);
        storage.insertItems(screwdriver, 5);

        cart.addItem(panda);
        cart.addItem(screwdriver);
        cart.removeItem(panda); // Odebereme pandu z košíku před nákupem.

        // Provedeme nákup a ověříme, že skladové zásoby a archivace odpovídají provedeným změnám.
        EShopController.purchaseShoppingCart(cart, "Karel Nový", "Dlouhá 100, Brno");
        assertEquals("Mělo by zůstat 4 šroubováků na skladě", 4, storage.getItemCount(screwdriver));
        assertEquals("Pandy by mělo zůstat 5 na skladě", 5, storage.getItemCount(panda));
        assertEquals("Šroubovák byl prodán 1x", 1, archive.getHowManyTimesHasBeenItemSold(screwdriver));
        assertEquals("Panda neměla být prodána", 0, archive.getHowManyTimesHasBeenItemSold(panda));
    }


    @Test
    public void testCompletePurchaseProcess() throws NoItemInStorage {
        ShoppingCart cart = new ShoppingCart();
        StandardItem panda = new StandardItem(1, "Dancing Panda", 5000, "GADGETS", 5);
        StandardItem screwdriver = new StandardItem(2, "Screwdriver", 200, "TOOLS", 5);

        // Přidáme položky do skladu
        storage.insertItems(panda, 5);
        storage.insertItems(screwdriver, 5);

        // Přidání položek do košíku
        cart.addItem(panda);
        cart.addItem(screwdriver);
        cart.addItem(screwdriver);

        // Odebrání jedné šroubováku před nákupem
        cart.removeItem(screwdriver);

        // Provedení nákupu
        EShopController.purchaseShoppingCart(cart, "Karel Nový", "Dlouhá 100, Brno");

        // Kontrola skladových zásob po nákupu
        assertEquals("Mělo by zůstat 4 pandy na skladě", 4, storage.getItemCount(panda));
        assertEquals("Mělo by zůstat 4 šroubováky na skladě", 4, storage.getItemCount(screwdriver));

        // Kontrola, zda byla objednávka správně archivována
        assertEquals("Panda byla prodána 1x", 1, archive.getHowManyTimesHasBeenItemSold(panda));
        assertEquals("Šroubovák byl prodán 1x", 1, archive.getHowManyTimesHasBeenItemSold(screwdriver));

        // Ověření, že jsou v archivu zaznamenány správné objednávky
        assertEquals("Měl by být zaznamenán jeden nákup", 1, archive.orderArchive.size());
    }
}
