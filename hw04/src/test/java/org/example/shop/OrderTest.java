package org.example.shop;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;

public class OrderTest {

    // Testování konstruktoru s plnými parametry
    @Test
    public void testFullConstructor() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "Test Item", 20.0f, "Electronics", 5));
        Order order = new Order(cart, "John Doe", "1234 Main St", 1);

        assertNotNull(order.getItems());
        assertEquals("John Doe", order.getCustomerName());
        assertEquals("1234 Main St", order.getCustomerAddress());
        assertEquals(1, order.getState());
    }

    // Testování konstruktoru s výchozím stavem
    @Test
    public void testDefaultStateConstructor() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "Test Item", 20.0f, "Electronics", 5));
        Order order = new Order(cart, "Jane Doe", "5678 Second St");

        assertNotNull(order.getItems());
        assertEquals("Jane Doe", order.getCustomerName());
        assertEquals("5678 Second St", order.getCustomerAddress());
        assertEquals(0, order.getState()); // Stav by měl být výchozí, tj. 0
    }

    // Testování konstruktoru s null hodnotami
    @Test(expected = NullPointerException.class)
    public void testConstructorWithNullCart() {
        new Order(null, "John Doe", "1234 Main St", 1);
    }

    @Test
    public void testConstructorWithNullName() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "Test Item", 20.0f, "Electronics", 5));
        Order order = new Order(cart, null, "1234 Main St", 1);

        assertNotNull(order.getItems());
        assertNull(order.getCustomerName());
        assertEquals("1234 Main St", order.getCustomerAddress());
        assertEquals(1, order.getState());
    }

    @Test
    public void testConstructorWithNullAddress() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "Test Item", 20.0f, "Electronics", 5));
        Order order = new Order(cart, "John Doe", null, 1);

        assertNotNull(order.getItems());
        assertEquals("John Doe", order.getCustomerName());
        assertNull(order.getCustomerAddress());
        assertEquals(1, order.getState());
    }
}
