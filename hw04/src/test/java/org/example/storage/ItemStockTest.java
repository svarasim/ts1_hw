package org.example.storage;

import org.example.shop.Item;
import org.example.shop.TestItem;

import org.junit.Test;

import static org.junit.Assert.*;


public class ItemStockTest {

    // Test konstruktoru ItemStock
    @Test
    public void testConstructor() {
        // Vytvoření instance TestItem
        Item item = new TestItem(1, "Test Item", 20.0f, "Electronics");
        // Vytvoření instance ItemStock s TestItem
        ItemStock stock = new ItemStock(item);

        // Ověření, že objekt item byl správně přiřazen do stock
        assertNotNull(stock.getItem());
        // Kontrola, že počet kusů na skladě je 0
        assertEquals(0, stock.getCount());
        // Ověření, že název itemu je "Test Item"
        assertEquals("Test Item", stock.getItem().getName());
    }
}
