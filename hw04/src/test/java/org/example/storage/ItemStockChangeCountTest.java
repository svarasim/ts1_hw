package org.example.storage;

import org.example.shop.Item;
import org.example.shop.TestItem;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class ItemStockChangeCountTest {

    private ItemStock stock;
    private int initialCount;
    private int change;
    private int expectedResult;

    // Konstruktor pro nastavení testovacích dat
    public ItemStockChangeCountTest(int initialCount, int change, int expectedResult) {
        this.initialCount = initialCount;
        this.change = change;
        this.expectedResult = expectedResult;
    }

    // Poskytuje data pro testy
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {0, 5, 5},  // Test zvyšování od 0 o 5
                {10, -3, 7},  // Test snižování z 10 o 3
                {3, -4, -1},  // Test snižování z 3 o 4, výsledkem je záporný počet
                {0, -1, -1},  // Test snižování z 0 o 1, výsledkem je záporný počet
                {20, 10, 30}  // Test zvyšování z 20 o 10
        });
    }

    // Nastavení prostředí před každým testem
    @Before
    public void setUp() {
        Item item = new TestItem(1, "Test Item", 20.0f, "Electronics");
        stock = new ItemStock(item);
        stock.IncreaseItemCount(initialCount);  // Nastavení počátečního počtu
    }

    // Testování změn počtu kusů
    @Test
    public void testChangeItemCount() {
        if (change >= 0) {
            stock.IncreaseItemCount(change);
        } else {
            stock.decreaseItemCount(-change);  // Záporné změny jsou převedeny na kladné pro snížení
        }
        // Ověření, že výsledný počet odpovídá očekávanému výsledku
        assertEquals(expectedResult, stock.getCount());
    }
}
