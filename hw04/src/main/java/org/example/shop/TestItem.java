package org.example.shop;

/**
 * TestItem is a concrete subclass of Item used for testing purposes.
 */
public class TestItem extends Item {

    public TestItem(int id, String name, float price, String category) {
        super(id, name, price, category);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
