package org.example;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SvarasimTest {
    @Test
    public void testFactorialRec() {
        assertEquals(1, Svarasim.factorialRecursive(0));
        assertEquals(1, Svarasim.factorialRecursive(1));
        assertEquals(2, Svarasim.factorialRecursive(2));
        assertEquals(6, Svarasim.factorialRecursive(3));
        assertEquals(120, Svarasim.factorialRecursive(5));
        assertEquals(3628800, Svarasim.factorialRecursive(10));
    }

    @Test
    public void testFactorial() {
        assertEquals(1, Svarasim.factorial(0));
        assertEquals(1, Svarasim.factorialRecursive(1));
        assertEquals(2, Svarasim.factorialRecursive(2));
        assertEquals(6, Svarasim.factorialRecursive(3));
        assertEquals(120, Svarasim.factorialRecursive(5));
        assertEquals(3628800, Svarasim.factorialRecursive(10));
    }


}
